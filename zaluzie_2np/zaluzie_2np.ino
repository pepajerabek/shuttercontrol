#include <shutter_unit.h>
#include <shutter_io.h>
#include <bus_pins_uno.h>
#include <pinout.h>
#include <platform.h>

const int PressedTimeoutMs = 1200;       // za jak dlouho od stisku udalost PRESSED
const unsigned long PullTimeMs = 60000;  // za jak dlouho sjede / vyjede žaluzie 
const unsigned long OpenTimeMs =  1200;  // potřebný čas na pootevření žaluzie sjeté 

// Pokoj levý, východní žaluzie 
const Pinout zapojeniPokojLevyVychod(
	BusDigital8Pin::D00, 
	BusDigital8Pin::D01,
	BusDigital10Pin::D10,
	BusDigital10Pin::D11
);

// Pokoj levý, jižní žaluzie 
const Pinout zapojeniPokojLevyJih(
	BusDigital8Pin::D02, 
	BusDigital8Pin::D03,
	BusDigital10Pin::D12,
	BusDigital10Pin::D13
);

// Pokoj pravý, jižní žaluzie 
const Pinout zapojeniPokojPravyJih(
	BusDigital8Pin::D04, 
	BusDigital8Pin::D05,
	BusAnalog::A0, 
	BusAnalog::A1
);

// Pokoj pravý, západní žaluzie 
const Pinout zapojeniPokojPravyZapad(
	BusDigital8Pin::D06, 
	BusDigital8Pin::D07,
	BusAnalog::A2, 
	BusAnalog::A3
);

// Koupelna
const Pinout zapojeniKoupelna(
	BusDigital10Pin::D08, 
	BusDigital10Pin::D09,
	BusAnalog::A4, 
	BusAnalog::A5
);

ShutterIO ioPokojLevyVychod(zapojeniPokojLevyVychod); 
ShutterIO ioPokojLevyJih(zapojeniPokojLevyJih); 
ShutterIO ioPokojPravyJih(zapojeniPokojPravyJih); 
ShutterIO ioPokojPravyZapad(zapojeniPokojPravyZapad); 
ShutterIO ioKoupelna(zapojeniKoupelna);

ShutterUnit zaluziePokojLevyVychod(ioPokojLevyVychod, PressedTimeoutMs, PullTimeMs, OpenTimeMs, 0);
ShutterUnit zaluziePokojLevyJih(ioPokojLevyJih, PressedTimeoutMs, PullTimeMs, OpenTimeMs, 1);
ShutterUnit zaluziePokojPravyJih(ioPokojPravyJih, PressedTimeoutMs, PullTimeMs, OpenTimeMs, 2);
ShutterUnit zaluziePokojPravyZapad(ioPokojPravyZapad, PressedTimeoutMs, PullTimeMs, OpenTimeMs, 3);
ShutterUnit zaluzieKoupelna(ioKoupelna, PressedTimeoutMs, PullTimeMs, OpenTimeMs, 4);


void setup() {
}


void loop() {
    
	unsigned long timeMs = Platform::GetTimeMs();

	zaluziePokojLevyVychod.OnTime(timeMs);
	zaluziePokojLevyJih.OnTime(timeMs);
	zaluziePokojPravyJih.OnTime(timeMs);
	zaluziePokojPravyZapad.OnTime(timeMs);
	zaluzieKoupelna.OnTime(timeMs);
}
