# Changelog

## Version 3.0

BUGFIX: Fixed time overflow every 65 seconds while mesuring duration.
NEWFEAT: Added function "DOWN AND OPEN".
REFACTOR: Platform provide PC/Arduino abstraction so that code above is platform independant.

## Version 2.1

Functions: UP_FULL, UP, DOWN_FULL, DOWN
