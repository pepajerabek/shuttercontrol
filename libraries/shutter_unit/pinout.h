#ifndef PINOUT_H
#define PINOUT_H


struct Pinout {        
    int SwitchUpPin;
    int SwitchDownPin;
    int PowerUpPin;
    int PowerDownPin; 

    Pinout(
        int switchUpPin,
        int switchDownPin, 
        int powerUpPin,
        int powerDownPin
    ) 
    : 
    SwitchUpPin(switchUpPin), 
    SwitchDownPin(switchDownPin),
    PowerUpPin(powerUpPin), 
    PowerDownPin(powerDownPin)
    {
    };
};

#endif // PINOUT_H

