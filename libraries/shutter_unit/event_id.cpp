#include "event_id.h"

char * GetEventName(EventId eventId)
{
// #ifndef ARDUINO  // not a big RAM on ARDUINO
    switch (eventId) {
        case EV_UP_CLICK:
            return("UP_CLICK");
        case EV_UP_PRESS:
            return("UP_PRESS");
        case EV_UP_RELEASE:
            return("UP_RELEASE");
        case EV_DN_CLICK:
            return("DN_CLICK");
        case EV_DN_PRESS:
            return("DN_PRESS");
        case EV_DN_RELEASE:
            return("DN_RELEASE");
        case EV_COUNT:
            return("COUNT");
        case EV_NONE:
            return("NONE");
    }
    return("NONE");
// #else
//     return("");
// #endif

};
