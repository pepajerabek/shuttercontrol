#ifndef SHUTTER_UNIT_H
#define SHUTTER_UNIT_H


// Complete control of one shutter (zaluzie)
#include "shutter_io.h"
#include "twin_button.h"
#include "state_machine.h"
#include "tranzition_map.h"

class ShutterUnit
{
public:
    /// Contructor
    /// @param pressedTimeoutMs - [ms] doba stisku tlačítka pro klasifikaci jako "držení"
    /// @param pullTimeMs - [ms] doba potřebná na sjetí / vytažení žaluzií
    /// @param openTimeMs - [ms] doba potřebná na pootevření žaluzie poté co sjela dolů
    ShutterUnit(
        ShutterIO & io,
        int pressedTimeoutMs,
        unsigned long pullTimeMs,
        unsigned long openTimeMs,
        unsigned char id,
        bool logEnabled = false
    );

    /// Time update
    /// @param timeMs - [ms] time from system start
    void OnTime(unsigned long timeMs);

private:
    const bool _logEnabled;

    ShutterIO & _shutterIo;
    TwinButton _twinButton;
    StateMachine _stateMachine;
    TranzitionMap _tranzitionMap;   
};


#endif // SHUTTER_UNIT_H
