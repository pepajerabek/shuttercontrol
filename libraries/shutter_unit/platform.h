// Hardware abstraction lever
// Abstraction for Aduino and PC (using "#define")
// Only one place for "#ifdef ARDUINO"

#ifndef HARDWARE_LAYER_H
#define HARDWARE_LAYER_H

class Platform {
public:
    enum SerialSpeedType {
        SP_4800 = 4800,
        SP_9600 = 9600,
        SP_14400 = 14400,
        // ... 
        SP_155200 = 155200
    };

    /// Initialize serial line for communication
    static void Init(
        SerialSpeedType serialSpeed = SP_9600,
        bool logEnabled = true
    );
    
    static void SetupPinInputPullup(int number);
    static void SetupPinOutput(int number);

    static void WritePin(int number, int value);
    static int ReadPin(int number);

    static unsigned long GetTimeMs();

    static void Print(char * str);
    static void Println(char * str);
    static void Print(unsigned long number);
    static void Println(unsigned long number);
};

#endif // HARDWARE_LAYER_H
