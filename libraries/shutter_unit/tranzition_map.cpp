#include "tranzition_map.h"
#include "platform.h"  // Print, Println


void TranzitionMap::SetEventTranzition(StateId stateId, EventId eventId, StateId nextStateId) {
    _tranzitions[stateId].NextStates[eventId] = nextStateId;
};


void TranzitionMap::SetDurationTranzition(
    StateId stateId,
    unsigned long nonIdleDurationMs,
    StateId nextState)
{
   	_tranzitions[stateId].NonIdleDurationMs = nonIdleDurationMs;
    _tranzitions[stateId].DurationNextState = nextState;
};


StateId TranzitionMap::GetNextStateEvent(StateId stateId, EventId eventId) const {
	return _tranzitions[stateId].NextStates[eventId];
};


StateId TranzitionMap::GetNextStateOnDuration(StateId stateId, 
        unsigned long nonIdleDurationMs) const 
{
    const Tranzition & tranzition = _tranzitions[stateId];
    if ( (tranzition.NonIdleDurationMs > 0) &&  
        (nonIdleDurationMs >= tranzition.NonIdleDurationMs) ) {
        return tranzition.DurationNextState;
    }
    else {
        return ST_NONE;
    }
};

void TranzitionMap::Print() {
#ifndef ARDUINO
    Platform::Println("Tranzition map:");
    Platform::Print("event ");
    for (int iEvent = 0; iEvent < EV_COUNT; ++iEvent) {
        Platform::Print(iEvent);
    }
    Platform::Println("");
    Platform::Print("state ------------");
    for (int iState = 0; iState < ST_COUNT; ++iState) {
        Platform::Print("  ");
        Platform::Print(iState);
        Platform::Print("|");
        Tranzition & tranzition = _tranzitions[iState];
        for (int iEvent = 0; iEvent < EV_COUNT; ++iEvent) {
            StateId nextStateId = tranzition.NextStates[iEvent];
            Platform::Print(nextStateId);
        }
        Platform::Println("");
    }
    Platform::Println("");
#endif
};

