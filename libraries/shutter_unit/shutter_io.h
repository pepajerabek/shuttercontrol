/// General interface working on Arduino and PC

#ifndef SHUTTER_IO_H
#define SHUTTER_IO_H

#include "pinout.h"
#include "io_state.h"


class ShutterIO
{
public:
    ShutterIO(const Pinout & pinout);
    IoState ReadSwitch();
    void WritePower(IoState state);
private:
    const Pinout & _pinout;
};


#endif // SHUTTER_IO_H

