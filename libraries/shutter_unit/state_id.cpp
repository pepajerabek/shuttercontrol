#include "state_id.h"


char * GetStateName(StateId stateId)
{
// #ifndef ARDUINO  // not a big RAM on ARDUINO
    switch (stateId) {
        case ST_IDLE:
            return("IDLE");
        case ST_OPEN_MAN:
            return("OPEN_MAN");
        case ST_CLOSE_MAN:
            return("CLOSE_MAN");
        case ST_PULL_UP:
            return("PULL_UP");
        case ST_PULL_DN:
            return("PULL_DN");
        case ST_PULL_DN_TO_OPEN:
            return("PULL_DN_TO_OPEN");
        case ST_PULL_DN_OPEN:
            return("PULL_DN_OPEN");
        case ST_COUNT:
            return("COUNT");
        case ST_NONE:
            return ("NONE");                    
    }
    return ("NONE");                    
// #else
//     return ("");
// #endif
};
