#include "state_machine.h"
#include "tranzition_map.h"

#include "platform.h" // Print, Println


StateMachine::StateMachine(
    TranzitionMap & tranzitionMap,
    StateId initialState,
    unsigned char id,
    bool logEnabled
    ): 
_tranzitionMap(tranzitionMap), 
_state(initialState), 
_id(id),
_logEnabled(logEnabled),
_nonIdleSetTimeMs(0)
//,_debugLastPrintedTime(0)
//,_debugLastPrintedDuration(0)
{
};


void StateMachine::OnTime(unsigned long timeMs) {
    unsigned long nonIdleDurationMs = _NonIdleDurationMs(timeMs);
// #if (_logEnabled)
//     unsigned int time = (unsigned int) timeMs / 1000;
//     if (_debugLastPrintedTime != time) {
//         Serial.print("SM");  
//         Serial.print(_id);
//         Serial.print(": time: ");
//         Serial.println(time);
//         std::cout << "SM" << _id << ": time: " << time << std::endl; 
//         _debugLastPrintedTime = time;
//     }    
//     unsigned int duration = (unsigned int) nonIdleDurationMs / 1000;
//     if (_debugLastPrintedDuration != duration) {
//         Serial.print("SM");  
//         Serial.print(_id);
//         Serial.print(": duration: ");
//         Serial.println(duration);
//         std::cout << "SM" << _id << ": duration: " << duration << std::endl; 
//         _debugLastPrintedDuration = duration;
//     }
// #endif
    StateId nextState = _tranzitionMap.GetNextStateOnDuration(_state, nonIdleDurationMs);
    if (nextState != _state) {
        _ChangeState(nextState, timeMs);
    }
};


void StateMachine::OnEvent(EventId eventId, unsigned long timeMs) {
    if (_logEnabled) {
        Platform::Print(Platform::GetTimeMs());
        Platform::Print(": SM");  
        Platform::Print(_id);
        Platform::Print(": event: ");
        Platform::Print(eventId);
        Platform::Print(" (");
        Platform::Print(GetEventName(eventId));
        Platform::Println(")");
    }
    // zde cely stavovy automat
    if (eventId == EV_NONE) {        
        return;
    }

    StateId nextState = _tranzitionMap.GetNextStateEvent(_state, eventId);
    if (nextState != _state) {
        _ChangeState(nextState, timeMs);
    }
};


StateId StateMachine::GetState() {
    return _state;
};


unsigned long StateMachine::_NonIdleDurationMs(unsigned long timeMs) {
    if (_state == ST_IDLE) {
        return 0;
    }
    else {
        // works also for the case when `timeMs` lower then `_nonIdleSetTimeMs`
        // https://www.norwegiancreations.com/2018/10/arduino-tutorial-avoiding-the-overflow-issue-when-using-millis-and-micros/ 
        return timeMs - _nonIdleSetTimeMs;
    }
};


void StateMachine::_ChangeState(StateId newState, unsigned long timeMs) {
    if (newState != ST_NONE) {
        if (_state == ST_IDLE && newState != ST_IDLE) {
            _nonIdleSetTimeMs = timeMs;
        }
        if (_logEnabled){
            Platform::Print(Platform::GetTimeMs());
            Platform::Print(": SM");
            Platform::Print(_id);
            Platform::Print(": state: ");
            Platform::Print(_state);
            Platform::Print(" (");
            Platform::Print(GetStateName(_state));
            Platform::Print(") -> ");
            Platform::Print(newState);
            Platform::Print(" (");
            Platform::Print(GetStateName(newState));
            Platform::Println(")");
        }
        _state = newState;
    }
}