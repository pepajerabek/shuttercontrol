#include "shutter_io.h"
#include "platform.h"


ShutterIO::ShutterIO(const Pinout &pinout) : _pinout(pinout)
{
  Platform::SetupPinInputPullup(_pinout.SwitchUpPin);
  Platform::SetupPinInputPullup(_pinout.SwitchDownPin);
  Platform::SetupPinOutput(_pinout.PowerUpPin);
  Platform::SetupPinOutput(_pinout.PowerDownPin);
};


IoState ShutterIO::ReadSwitch() 
{
    IoState state = IO_NONE;
    int up = !Platform::ReadPin(_pinout.SwitchUpPin);
    int down = !Platform::ReadPin(_pinout.SwitchDownPin);
    if (up & down) {     
      state = IO_BOTH;
    }
    else if (up) {
      state = IO_UP;
    }
    else if (down) {
      state = IO_DOWN;
    }
    else {
      state = IO_NONE;
    }
    return state;
};


void ShutterIO::WritePower(IoState state){
    int up = 0;
    int down = 0;

    if ( (state == IO_BOTH) || (state == IO_UP) ) { 
      up = 1;
    }
    if ( (state == IO_BOTH) || (state == IO_DOWN) ) { 
      down = 1;
    }

    Platform::WritePin(_pinout.PowerUpPin, up);
    Platform::WritePin(_pinout.PowerDownPin, down);
};
