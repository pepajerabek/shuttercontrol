#include "platform.h"
#ifdef ARDUINO
#include <Arduino.h>
/// library "memory_free"
#include <MemoryFree.h>
#else
#include <iostream>
#endif


void Platform::Init(
        SerialSpeedType serialSpeed,
        bool logEnabled
    )
{
    if (logEnabled) {
	#ifdef ARDUINO
 	   // POZOR: zapisuje do vstupů D00 a D01
 	   Serial.begin(9600);  // debug prints inside
 	   delay(1000);
 	   Serial.print("freeMemory=");
 	   Serial.println(freeMemory());
    #else
	#endif
	}     
};


void Platform::SetupPinInputPullup(int number){
    #ifdef ARDUINO
    if (number >= 0) {
        pinMode(number, INPUT_PULLUP);
    }
    #else
        std::cout << "Setting input pullup pin: " << number << std::endl;
    #endif
};


void Platform::SetupPinOutput(int number){
    #ifdef ARDUINO
    if (number >= 0) {
        pinMode(number, OUTPUT);
    }
    #else
        std::cout << "Setting output pin: " << number << std::endl;
    #endif
};


void Platform::WritePin(int number, int value){
#ifdef ARDUINO
    if (number >= 0) {
        digitalWrite(number, value);
    }
#endif
};


int Platform::ReadPin(int number) {
    #ifdef ARDUINO
    if (number >= 0) {
        return digitalRead(number);
    }
    #else
    return -1;
    #endif
};


unsigned long Platform::GetTimeMs() {
    #ifdef ARDUINO
    return millis();
    #endif
    return 0; // TODO
};


void Platform::Print(char * str) {
    #ifdef ARDUINO
    Serial.print(str);
    #else
    std::cout << str;
    #endif
};


void Platform::Println(char * str) {
    #ifdef ARDUINO
    Serial.println(str);
    #else
    std::cout << str << std::endl;
    #endif
};


void Platform::Print(unsigned long number) {
    #ifdef ARDUINO
    Serial.print(number);
    #else
    std::cout << number;
    #endif
};


void Platform::Println(unsigned long number) {
    #ifdef ARDUINO
    Serial.println(number);
    #else
    std::cout << number << std::endl;
    #endif
};
   
   
   
   