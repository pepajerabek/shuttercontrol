#include "shutter_unit.h"

#include "tranzition_map_fill_custom.h"


IoState outputFunction(StateId state) {
	if (state == ST_OPEN_MAN || state == ST_PULL_UP || state == ST_PULL_DN_OPEN) {
		return IO_UP;
	}
	else if (state == ST_CLOSE_MAN || state == ST_PULL_DN || state == ST_PULL_DN_TO_OPEN) {
		return IO_DOWN;
	}	
    return IO_NONE;
};

ShutterUnit::ShutterUnit(
    ShutterIO & shutterIo,
    int pressedTimeoutMs,
    unsigned long pullTimeMs,
    unsigned long openTimeMs,
    unsigned char id,
    bool logEnabled
)
:
_stateMachine(_tranzitionMap, ST_IDLE, id, logEnabled),
_shutterIo(shutterIo),
_twinButton(pressedTimeoutMs),
_logEnabled(logEnabled)
{   
    tranzitionMapFillCustom(_tranzitionMap, pullTimeMs, openTimeMs);
}


void ShutterUnit::OnTime(unsigned long timeMs)
{
    IoState switchIoState = _shutterIo.ReadSwitch();
    EventId eventId = _twinButton.OnTime(timeMs, switchIoState);
    
    if (eventId != EV_NONE) {
        _stateMachine.OnEvent(eventId, timeMs);
    } else {
        _stateMachine.OnTime(timeMs);
    }
    
    StateId state = _stateMachine.GetState();

// NOTE: state can be printed out also in StateMachine
// use Platform for prints!
//     printf("time=%06d switch=%s event=%s new_stateid=%s\n", 
//             (unsigned int) timeMs,
//             GetIoStateName(switchIoState).c_str(), 
//             GetEventName(eventId).c_str(),
//             GetStateName(state).c_str());
    IoState out = outputFunction(state);
    _shutterIo.WritePower(out);
    
};

