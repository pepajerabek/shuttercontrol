/// library "shutter_unit"
#include <shutter_unit.h>
#include <shutter_io.h>
#include <bus_pins_uno.h>
#include <pinout.h>
#include <platform.h>


const int PressedTimeoutMs = 1200;       // za jak dlouho od stisku udalost PRESSED
const unsigned long PullTimeMs = 60000;  // za jak dlouho sjede / vyjede žaluzie 
const unsigned long OpenTimeMs =  1200;  // potřebný čas na pootevření žaluzie sjeté dolů

// zapnuti vypisu (Arduino serial port, PC consola)
const bool LogEnabled = false;


// Obyvak leve jizni okno
const Pinout zapojeniObyvakVlevo(
	BusDigital10Pin::D10, 
	BusDigital10Pin::D11,
	BusAnalog::A0, 
	BusAnalog::A1
);
  
// Obyvak prave jizni okno
const Pinout zapojeniObyvakVpravo(
	BusDigital8Pin::D02, 
	BusDigital8Pin::D03,
	BusAnalog::A2, 
	BusAnalog::A3
);

// Obyvak smer terasa
const Pinout zapojeniObyvakTerasa(
	BusDigital8Pin::D04, 
	BusDigital8Pin::D05,
	BusAnalog::A4, 
	BusAnalog::A5
);

const Pinout zapojeniPracovna(
	BusDigital8Pin::D06, 
	BusDigital8Pin::D07,
	BusDigital10Pin::D08,
	BusDigital10Pin::D09
);

ShutterIO ioObyvakVlevo(zapojeniObyvakVlevo);
ShutterIO ioObyvakVpravo(zapojeniObyvakVpravo);
ShutterIO ioObyvakTerasa(zapojeniObyvakTerasa);
ShutterIO ioPracovna(zapojeniPracovna);

    
ShutterUnit zaluzieObyvakVlevo(
	ioObyvakVlevo, 
	PressedTimeoutMs, 
	PullTimeMs, 
	OpenTimeMs, 
	0,
	LogEnabled);
ShutterUnit zaluzieObyvakVpravo(
	ioObyvakVpravo,
	PressedTimeoutMs,
	PullTimeMs,
	OpenTimeMs,
	1,
	LogEnabled);
ShutterUnit zaluzieObyvakTerasa(
	ioObyvakTerasa,
	PressedTimeoutMs,
	PullTimeMs,
	OpenTimeMs,
	2,
	LogEnabled);
ShutterUnit zaluziePracovna(
	ioPracovna,
	PressedTimeoutMs,
	PullTimeMs,
	OpenTimeMs,
	3,
	LogEnabled);


void setup() {
	Platform::Init(Platform::SP_9600, LogEnabled);
}


void loop() {
    unsigned long timeMs = Platform::GetTimeMs();

	zaluzieObyvakVlevo.OnTime(timeMs);
	zaluzieObyvakVpravo.OnTime(timeMs);
	zaluzieObyvakTerasa.OnTime(timeMs);
	zaluziePracovna.OnTime(timeMs);
}
